package phi.resolver;
import phi.exceptions.*;
import phi.checkexpression.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Scanner;

public class MainClass extends Exception{
    public static void main(String[] args) throws DivByZero, DoesntIntType, DuplicateOperator, OutOfCharacterSet, NotInfix, RedundantCloseBracket, RedundantOpenBracket, MissingOperatorInBracket, InvalidExpression {
        /*String exp = "3+2";
        ResolvePostfix pfv = new ResolvePostfix(exp);
        double result = pfv.evaluatePostfix();
        System.out.println(new BigDecimal(result, new MathContext(0)));*/

        while(true){
            try{
                Scanner kb = new Scanner(System.in);
                String exp = kb.nextLine();
                ResolvePostfix pfv = new ResolvePostfix(exp);
                double result = pfv.evaluatePostfix();
                System.out.println(new BigDecimal(result, new MathContext(0)));
            }catch(DivByZero e){
                System.out.println(e.toString());
            }catch(DoesntIntType e){
                e.toString();
                System.out.println(e.toString());
            }catch(DuplicateOperator e){
                System.out.println(e.toString());
            }catch(MissingOperatorInBracket e){
                System.out.println(e.toString());
            }catch(NotInfix e){
                System.out.println(e.toString());
            }catch(OutOfCharacterSet e){
                System.out.println(e.toString());
            }catch (InvalidExpression e){
                System.out.printf(e.toString());
            }
            catch(RedundantOpenBracket e){
                System.out.println(e.toString());
            }catch(RedundantCloseBracket e){
                System.out.println(e.toString());
            }
            catch (Exception e){
                e.toString();
            }
        }
    }
}
