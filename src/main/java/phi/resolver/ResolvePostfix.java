package phi.resolver;

import phi.entity.Calculator;
import phi.resolver.PostfixConverter;
import phi.exceptions.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ResolvePostfix {
    private String userText;

    public ResolvePostfix(String userText) {
        this.userText = userText;
    }

    public String getUserText() {
        return userText;
    }

    public void setUserText(String userText) {
        this.userText = userText;
    }

//    List<String> postfix = new ArrayList<String>();

    public double evaluatePostfix() throws DivByZero, DuplicateOperator, OutOfCharacterSet,
            NotInfix, DoesntIntType, MissingOperatorInBracket, RedundantCloseBracket, RedundantOpenBracket, InvalidExpression {


        List<String> postfix = new ArrayList<String>();
        postfix = PostfixConverter.convertToPostfix(this.getUserText());
        Stack<Double> stack = new Stack<Double>();
        for (int i = 0; i != postfix.size(); i++) {
            if (Character.isDigit(postfix.get(i).charAt(0))) {
                stack.push(Double.valueOf(postfix.get(i)));
            } else {
                double op1 = stack.pop();
                double op2 = stack.pop();
                // switch (postfix.get(i)) {
                switch (postfix.get(i)) {
                    case "+": {
                        stack.push(Calculator.add(op2, op1));
                        break;
                    }
                    case "-": {
                        stack.push(Calculator.sub(op2, op1));
                        break;
                    }
                    case "*": {
                        stack.push(Calculator.mul(op2, op1));
                        break;
                    }
                    case "/": {
                        //exception
                        if(op1==0)
                            throw new DivByZero();
                        stack.push(Calculator.div(op2, op1));
                        break;
                    }
                }
            }
        }
        return stack.pop();
    }
}