package phi.resolver;
import phi.checkexpression.CheckEx;
import phi.exceptions.*;
import sun.security.rsa.RSAUtil;

import java.util.*;

public class PostfixConverter {
    String expression;
    public static boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '^' || c == '(' || c == ')';
    }

    public static int precedence(char c) {
        if (c == '^')
            return 4;
        else if (c == '*' || c == '/')
            return 3;

        else if (c == '+' || c == '-')
            return 2;

        else if (c == '(') {
            return 1;
        } else {
            return 0;
        }
    }

    public static List<String> convertToPostfix(String exp) throws NotInfix, DoesntIntType, MissingOperatorInBracket, DuplicateOperator, OutOfCharacterSet, InvalidExpression, RedundantCloseBracket, RedundantOpenBracket {
        exp = exp.replaceAll(" ", "");
        exp = exp.replace("+-", "-");
        exp = exp.replace("-+", "-");
        exp = exp.replace("++", "+");
        exp = exp.replace("--", "+");
        exp = exp.replace(")(", ")*(");
        if (exp.charAt(0) == '+' || exp.charAt(0) == '-')
            exp = '0' + exp;
        // convert in below: :D
        if(CheckEx.check(exp) ==true) {
            System.out.println();
            System.out.println("Right");
        }else{
            System.out.println("your expression is invalid");
        }
        Stack<Character> stOperand = new Stack<Character>();
        StringBuffer sbPostfix = new StringBuffer("");
        char ch;
        int count = 0;

        String str = "";
        for (int i = 0; i < exp.length(); i++) {
            ch = exp.charAt(i);
            if (!isOperator(ch)) {
                sbPostfix.append(ch);
                continue;
            }
            sbPostfix.append(" ");
            {
                if (ch == ')') {

                    while (!stOperand.isEmpty() && stOperand.peek() != '(') {
                        sbPostfix.append(" ").append(stOperand.pop());
                    }

                    if (!stOperand.isEmpty()) {
                        stOperand.pop();
                    }
                } else {

                    if (!stOperand.isEmpty() && precedence(ch) > precedence(stOperand.peek())) {
                        stOperand.push(ch);
                    } else {

                        if (stOperand.isEmpty()) {

                        } else if (!stOperand.isEmpty() && ch == '(') {

                        } else if (!stOperand.isEmpty()
                                && precedence(ch) <= precedence(stOperand.peek())) {

                            sbPostfix.append(stOperand.pop()).append(" ");
                        }
                        stOperand.push(ch);
                    }

                }
            }
        }
        while (!stOperand.isEmpty()) {
            sbPostfix.append(" ").append(stOperand.pop());
        }
        List<String> list = new ArrayList<String>(Arrays.asList(sbPostfix.toString().trim().split(" ")));
        for (int a = 0; a < list.size(); a++) {
            if (list.get(a).equals("")) {
                list.remove(a);
            }
        }
        return list;
    }
//    public static void main(String[] args){
//        PostfixConverter inf = new PostfixConverter();
//        List<String> str = new ArrayList<String>();
//        str = PostfixConverter.convertToPostfix("(32-2)*10");
//        System.out.println(str);
//    }
}