package phi.checkexpression;

import phi.exceptions.*;
import phi.resolver.PostfixConverter;
import java.util.Stack;

public class CheckEx {
    private static int precedence(char c) {
        if (c == '^')
            return 4;
        else if (c == '*' || c == '/')
            return 3;

        else if (c == '+' || c == '-')
            return 2;

        else if (c == '(') {
            return 1;
        } else {
            return 0;
        }
    }

    public static boolean check(String expInput) throws MissingOperatorInBracket, DuplicateOperator, OutOfCharacterSet, DoesntIntType, NotInfix, RedundantOpenBracket, RedundantCloseBracket {
        expInput = expInput.replaceAll(" ", "");
        expInput = expInput.replace("+-", "-");
        expInput = expInput.replace("-+", "-");
        expInput = expInput.replace("++", "+");
        expInput = expInput.replace("--", "+");
        expInput = expInput.replace(")(", ")*(");

        // My check exception
        // check first

        if (expInput.charAt(0) == '*' || expInput.charAt(0) == '/')
            throw new NotInfix();
        for (int i = 0; i < expInput.length(); i++) {
            char c = expInput.charAt(i);
            if (c == '.' || c == ',') { // dấu chấm, dấu chấm phẩy
                throw new DoesntIntType();
            } else if (!((c >= 48 && c <= 57) || c == '+' || c == '-' || c == '*' || c == '/' || c == '(' || c == ')')) {
                throw new OutOfCharacterSet();
            } else if (c == '/' || c == '*') {
                if (expInput.charAt(i + 1) == '/' || expInput.charAt(i + 1) == '*' || expInput.charAt(i + 1) == '+' || expInput.charAt(i + 1) == '-') {
                    throw new DuplicateOperator(i + 1);
                }
            } else if (c == '-' || c == '+') {
                if (expInput.charAt(i + 1) == '/' || expInput.charAt(i + 1) == '*') {
                    throw new DuplicateOperator(i + 1);
                }
            } else if ((i + 1 < expInput.length()) && (c == 40) && (expInput.charAt(i + 1) == 41)) {
                throw new MissingOperatorInBracket();
            } else {

            }
        }
        Stack<Character> stack = new Stack<Character>();
        String result = new String(""); // initializing empty String for result
        for (int i = 0; i < expInput.length(); ++i) {
            char c = expInput.charAt(i);
            if (!((c >= 48 && c <= 57) || c == '+' || c == '-' || c == '*' || c == '/' || c == '(' || c == ')'))
                throw new OutOfCharacterSet();
            if (Character.isDigit(c)) {
                result += c;
            } else if (c == '(') {
                stack.push(c);
            } else if (c == ')') {
                while (!stack.isEmpty() && stack.peek() != '(') {
                    result += stack.pop();
                    if (stack.isEmpty() == true) {
                        throw new RedundantCloseBracket();
                    }
                }
                stack.pop();
            } else // an operator is encountered- + - *
            {
                while (!stack.isEmpty() && PostfixConverter.precedence(c) <= PostfixConverter.precedence(stack.peek())) {
                    result += stack.pop();
                }
                stack.push(c);
            }
        }

        // pop all the operators from the stack
        while (!stack.isEmpty()) {
            if (stack.peek() == '(') {
                throw new RedundantOpenBracket();
            }
            result += stack.pop();
        }
        return true;
    }
}
