package phi.exceptions;

public class DuplicateOperator extends Exception{
	int i;
	public DuplicateOperator(int i) {
		super();
		this.i = i;
	}
	@Override
	public String toString() {
		return "You don't have to duplicate operator at "+ this.i;
	}
}
