package phi.exceptions;

public class InvalidExpression extends Exception{
    public InvalidExpression(){
        super();
    }
    @Override
    public String toString() {
        return "Your expressions is invalid";
    }
}
