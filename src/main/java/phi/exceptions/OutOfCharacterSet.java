package phi.exceptions;

public class OutOfCharacterSet extends Exception {

	public OutOfCharacterSet() {
		super();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Out of character set in your expressions";
	}
}
