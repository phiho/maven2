package test;

import org.junit.Test;
import phi.exceptions.*;
import phi.resolver.ResolvePostfix;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Test2 {
    @Test(expected = RedundantOpenBracket.class)
    public void redundantOpenBracket1() throws Exception {
        ResolvePostfix text1 = new ResolvePostfix("((3+2-6*(1+2)");
        text1.evaluatePostfix();
    }

    @Test(expected = RedundantOpenBracket.class)
    public void redundantOpenBracket2() throws Exception {
        ResolvePostfix text2 = new ResolvePostfix("(3+2-6(*(1+2)");
        text2.evaluatePostfix();
    }

    @Test(expected = RedundantCloseBracket.class)
    public void redundantCloseBracket1() throws Exception {
        ResolvePostfix text = new ResolvePostfix("3-6(*(1+2)))");
        text.evaluatePostfix();
    }

    @Test(expected = RedundantCloseBracket.class)
    public void redundantCloseBracket2() throws Exception {
        ResolvePostfix text = new ResolvePostfix("1+2*(5+2))");
        text.evaluatePostfix();
    }

    @Test
    public void precedenceInBracket1() throws Exception { // tinh trong ngoac truoc
        ResolvePostfix text = new ResolvePostfix("1-(5+2)");
        Double actual = text.evaluatePostfix();
        assertEquals(-6, actual, 0.001);
    }

    @Test
    public void precedenceInBracket2() throws Exception { // tinh trong ngoac truoc
        ResolvePostfix text = new ResolvePostfix("((5+2)-7)*6");
        double actual = text.evaluatePostfix();
        assertEquals(0, actual);
    }

    @Test
    public void precedenceInBracket3() throws Exception { // tinh trong ngoac truoc
        ResolvePostfix text = new ResolvePostfix("(4*3)/4");
        double actual = text.evaluatePostfix();
        assertEquals(3, actual);
    }

    public void precedenceOperator1() throws Exception { // nhan chia truoc
        ResolvePostfix text = new ResolvePostfix("1+2*3");
        double actual = text.evaluatePostfix();
        assertEquals(7, actual);
    }

    @Test(expected = DivByZero.class)
    public void divByZero1() throws Exception {
        ResolvePostfix text = new ResolvePostfix("1+2*(5+2)/0");
        text.evaluatePostfix();
    }

    @Test(expected = DivByZero.class)
    public void divByZero2() throws Exception {
        ResolvePostfix text = new ResolvePostfix("1+2/((8-9)*0)");
        text.evaluatePostfix();
    }

    @Test(expected = DivByZero.class)
    public void divByZero3() throws Exception {
        ResolvePostfix text = new ResolvePostfix("1+2/(3-3)");
        text.evaluatePostfix();
    }

    @Test(expected = DoesntIntType.class)
    public void doesntIntType1() throws Exception {
        ResolvePostfix text = new ResolvePostfix("1,4+2*(5+2)");
        text.evaluatePostfix();
    }

    @Test(expected = DoesntIntType.class)
    public void doesntIntType2() throws Exception {
        ResolvePostfix text = new ResolvePostfix("1.4+2*(5+2)");
        text.evaluatePostfix();
    }

    @Test(expected = DuplicateOperator.class)
    public void duplicateOperator1() throws Exception {
        ResolvePostfix text = new ResolvePostfix("4+2*/(5+2)");
        text.evaluatePostfix();
    }

    @Test(expected = DuplicateOperator.class)
    public void duplicateOperator2() throws Exception {
        ResolvePostfix text = new ResolvePostfix("4+2-/(5+2)");
        text.evaluatePostfix();
    }

    @Test(expected = DuplicateOperator.class)
    public void duplicateOperator3() throws Exception {
        ResolvePostfix text = new ResolvePostfix("4+2-*+(5+2)");
        text.evaluatePostfix();
    }

    @Test(expected = MissingOperatorInBracket.class)
    public void missingOperatorInBracket() throws Exception {
        ResolvePostfix text = new ResolvePostfix("4()+2*(5+2)");
        text.evaluatePostfix();
    }

    @Test
    public void test15() throws Exception { // fail
        ResolvePostfix text = new ResolvePostfix("4(*)3");
        double actual = text.evaluatePostfix();
        assertEquals(12, actual);
    }

    @Test(expected = OutOfCharacterSet.class)
    public void outOfCharacterSet1() throws Exception {
        ResolvePostfix text = new ResolvePostfix("a+2*(5+2)");
        text.evaluatePostfix();
    }

    @Test(expected = OutOfCharacterSet.class)
    public void outOfCharacterSet2() throws Exception {
        ResolvePostfix text = new ResolvePostfix("&+2*(5+2)");
        text.evaluatePostfix();
    }

    @Test
    public void amDuong() throws Exception {
        ResolvePostfix text = new ResolvePostfix("+9*(5+2)");
        double actual = text.evaluatePostfix();
        assertEquals(63, actual);
    }
    @Test
    public void amDuong2() throws Exception {
        ResolvePostfix text = new ResolvePostfix("-9*(5+2)");
        double actual = text.evaluatePostfix();
        assertEquals(-63, actual);
    }
    @Test
    public void amDuong3() throws Exception {
        ResolvePostfix text = new ResolvePostfix("+-9*(5+2)");
        double actual = text.evaluatePostfix();
        assertEquals(-63, actual);
    }
    @Test
    public void amDuong4() throws Exception {
        ResolvePostfix text = new ResolvePostfix("++9*(5+2)");
        double actual = text.evaluatePostfix();
        assertEquals(63, actual);
    }

    @Test(expected = NotInfix.class)
    public void notInfix2() throws Exception {
        ResolvePostfix text = new ResolvePostfix("*9/(5+2)");
        double actual = text.evaluatePostfix();
        assertEquals(63, actual);
    }

    @Test(expected = NotInfix.class)
    public void notInfix3() throws Exception {
        ResolvePostfix text = new ResolvePostfix("/9*(5+2)");
        double actual = text.evaluatePostfix();
        assertEquals(63, actual);
    }

    @Test
    public void beforeBracket1() throws Exception {
        ResolvePostfix text = new ResolvePostfix("-(5+2)");
        double actual = text.evaluatePostfix();
        assertEquals(-7, actual);
    }

    @Test
    public void congTruLienKe1() throws Exception { // fail
        ResolvePostfix text = new ResolvePostfix("3+-2");
        double actual = text.evaluatePostfix();
        assertEquals(1, actual);
    }
    @Test
    public void congTruLienKe2() throws Exception { // fail
        ResolvePostfix text = new ResolvePostfix("(3+1)+-(2+2)");
        double actual = text.evaluatePostfix();
        assertEquals(0, actual);
    }

    @Test
    public void complexExpression1() throws Exception {
        ResolvePostfix text = new ResolvePostfix("(8+2)/(7+3)");
        double actual = text.evaluatePostfix();
        assertEquals(1, actual);
    }

    @Test
    public void complexExpression2() throws Exception { // pass
        ResolvePostfix text = new ResolvePostfix("(8+1)/(3)");
        double actualResult = text.evaluatePostfix();
        assertEquals(3, actualResult);
    }

    @Test
    public void complexExpression3() throws Exception { // pass
        ResolvePostfix text = new ResolvePostfix("((8+2)*9)/(9+1)");
        double actualResult = text.evaluatePostfix();
        assertEquals(9, actualResult);
    }
}